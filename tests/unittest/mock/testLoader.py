"""
Loader Tests
============

:mod:`importlib` provides various *Loader* implementations.
This suite of tests are used to explore their functionality.
Similarly these are compared to the functionality of the :class:`MockLoader`.

As these tests are fleshed out one will use them to assert various :mod:`importlib` behaviour.
"""

from unittest import TestCase, main

class LoaderTests(TestCase) :
 pass
 
if __name__ == "__main__" :
 import sys
 from pprint         import pprint
 from pathlib        import Path
 from importlib.util import find_spec 
 sys.path.append(str(Path("../../mock_mockup").resolve()))
 spec = find_spec("example")
#  pprint(dir(spec))
#  pprint(spec)