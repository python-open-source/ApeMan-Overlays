"""
Test Mock
=========

This module tests the behaviour of the :meth:`patch.module` decorator provided by the :mod:`unittest.mock` overlay.
"""
import sys
import overlays
from pathlib       import Path                            # Note : The pathlib overlay is imported
from unittest      import TestCase, main, expectedFailure
from unittest.mock import patch
# from apeman.mock   import module # Handled implicitly in the line above

MOCK = Path(__file__).resolve().parents[3]/"mockup"

class testMockedModules(TestCase) :
 """
 This ensures that the mocked module has a similar looking set of attributes as a real one.
 """

 @expectedFailure # MockModule does not set the __file__ or __cached__ entries for a module
 @patch("sys.path", [str(MOCK)] + sys.path.copy())
 def testComparingAttributes(self = None) :
  """Ensure :class:`MockModule` and :class:`types.ModuleType` have similar attributes.
  
  .. note ::
  
     Due to the manner in which the :meth:`module` (Strictly :meth:`MetaMockFinder`) generates the mocked module the
     __file__ and __cached__ attributes are not properly setup. The current solution prefixes the provided code with
     an assignment, setting the :attr:`__file__` attribute, roughly equivalent to the following code.
     ::
       
       code = "__file__ = '{}'".format(path) + code
     
     One has not determined the best means of mock'ing an assignment to :attr:`__cached__`, also one is not sure one 
     should. Atleast doing so might cause Python to look for some byte code file that does not exist creating more 
     problems then such as assignment hopes to solve.
  """
  @patch("sys.modules", sys.modules.copy())
  def answer() : 
   import empty
   return dir(empty)
  with self.subTest("Failed to load the real module") :
   answer()
  @patch.module("empty", "empty.py")
  @patch("sys.modules", sys.modules.copy())
  def result() :
   import empty
   return dir(empty)
  with self.subTest("Failed to load the mock module") :
   result()
  self.assertEqual(set(answer()), set(result()))

 @patch("sys.path", [str(MOCK)] + sys.path.copy())
 def testRealEmptymodule(self = None) :
  """Test that the empty module in the mockups folder is accessible"""
  @patch("sys.modules", sys.modules.copy())
  def answer() : 
   import empty
   return dir(empty)
  answer()

 def testMockEmptymodule(self = None) :
  """Test that the empty module in the mockups folder is accessible"""
  @patch.module("empty", "empty.py")
  @patch("sys.modules", sys.modules.copy())
  def result() :
   import empty
   return dir(empty)
  result()
  
@patch("sys.modules", sys.modules.copy())
class testAncestry(TestCase) :
 """
 Ensure that the parent packages for a module are created properly.
 """

 @patch.module("package.module")
 def testPackage(self) :
  import package

 @patch.module("package.module")
 def testModule(self) :
  import package.module

 @patch.module("package.module")
 def testModuleFromPackage(self) :
  from package import module

if __name__ == "__main__":
 main()
