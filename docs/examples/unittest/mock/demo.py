"""
Simple demonstration showing how to perform stateless imports while unit testing.
"""
from unittest.mock import patch
from apeman.mock   import MetaMockFinder
import sys


packageName = "package.module"
packagePath = None
packageCode = """
from pprint import pprint
class Class() : 
 def __init__(self, *args, **kvps) : 
  pprint(dir(self))
  pprint(self.__module__)
  pprint(self.__class__)"""
 
@mockModule(packageName,packagePath,packageCode)
@patch("sys.modules", sys.modules.copy())
def function() :
 old = set(sys.modules.keys())
 spec = next(mpf.find_spec(packageName) for mpf in sys.meta_path)
#  print(spec)
#  print(spec.loader.get_source(packageName))
 from package.module import Class
 Class()
 new = set(sys.modules.keys())
 print("Locally  :",new-old)

old = set(sys.modules.keys())
function()
new = set(sys.modules.keys())
print("Globally :",new-old)

print(MetaMockFinder() == MetaMockFinder())