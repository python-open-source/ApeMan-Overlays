Original
========

.. toctree::
   :titlesonly:
   :maxdepth: 3
   :glob:
   
   original/*


This contains the original implementation of the package and is retained for the moment so that the code may be scavanged for the good bits.
For historic reasons it also contains the first Apeman implementation that actually worked and did not look like Jacob Zuma's resignation letter.

Module contents
---------------

.. automodule:: original
    :members:
    :undoc-members:
    :show-inheritance:

