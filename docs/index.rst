.. Apeman : Overlays documentation master file, created by
   sphinx-quickstart on Fri Sep 15 00:05:45 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

==================
Apeman : Overlays!
==================

.. toctree::
   :maxdepth: 4
   :hidden:

   overlays
   original
   setup
   example

.. Use either a pull-quote or an epigraph here

.. pull-quote ::

   "There is a little bird singing on my window sill;
    I pretend I'm asleep, but it's singing there still."
                                   
   --  Koos Kombuis

.. only :: builder_html

   .. include :: ../readme.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
