from functools import wraps
from apeman.mock import MockFinder, MockModule

def module(name, code = None, path = None, root = None) :
 MockFinder(MockModule(name, code =code, path = path, root = root))
 def wrapper(fxn):
 return (fxn)

code = """
class A() :
 def __str__(self) :
  return "Frikking A!"
"""

@module("package.module", code = code)
@patch('sys.modules', sys.modules.copy())
def testImport():
 from package.module import A
 print(A())

# from unittest.mock import patch
# import sys
# 
# @patch('sys.modules', sys.modules.copy())
# def testImport():
#  oldkeys = set(sys.modules.keys())
#  import MODULE
#  newkeys = set(sys.modules.keys())
#  print((newkeys)-(oldkeys))
#  
# oldkeys = set(sys.modules.keys())
# testImport()
# newkeys = set(sys.modules.keys())
# print((newkeys)-(oldkeys))
