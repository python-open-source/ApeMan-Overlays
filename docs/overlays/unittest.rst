Unittest
========

.. toctree::
   :titlesonly:
   :maxdepth: 3
   :glob:
   
   unittest/*

This is a bit of a tricky one.
It seems that :code:`from unittest import *` imports two :class:`TestCase` subclasses that interfere with any test discovery and one must explicitly import the features they are interested in.
This makes it a bit tricky for ApeMan which typically relies upon the ``*`` imports to work properly.

.. automodule :: overlays._unittest_
  :members:         
  :undoc-members:   
  :show-inheritance:

