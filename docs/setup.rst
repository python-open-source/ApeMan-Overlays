Contribution
============

This section covers how others may get setup to contribute.

.. automodule:: setup
    :members:
    :undoc-members:
    :show-inheritance:
