"""
--------------
Relationshsips
--------------

These tests cover the behaviour under repeated imports both within the same module and across multiple modules.

.. note ::

   These tests presently rely upon the ApeMan-Overlay they should be made more indepepndant in teh future.
"""
# System tools
import os, sys
# Testing Tools
from unittest import TestCase, main
from unittest import mock
# Path Handling
from pathlib import Path
# Utilities
# from .utils.execute import osexec

# Not sure if this should be made part of nosejob ?
import types

def module(file, name, code) :
 # Create a module
 #
 # __name__    : Name
 # __file__    : Source file if any
 # __doc__     : documentation string
 # __path__    : package path
 # __package__ : package name
 # _spec__     : modulespec
 mod = types.ModuleType(name)
 src = compile(code, file, "exec")
 exec(src, mod.__dict__)
 return mod

# class testRepeatedImports(TestCase) :
#  """Tests the explicitly imported overlay"""
# 
#  #: Mask provides a code snippet
#  mask = """
# import json
# import logging; logging.basicConfig(level = logging.DEBUG)
# import overlays
# # import sys
# # print([mod for mod in sys.modules if "pathlib" in mod])
# from pathlib import Path as NewPath
# # print([mod for mod in sys.modules if "pathlib" in mod])
# from pathlib import Path as OldPath
# # print([mod for mod in sys.modules if "pathlib" in mod])
# print(NewPath == OldPath)
# print(hasattr(OldPath, 'temp'))
# print(hasattr(NewPath, 'temp'))
#  """
#  with patch("module")
#  #: Path provides a root folder within which to execute the code
#  path = str(Path(os.path.dirname(os.path.abspath(__file__))).joinpath('../mockup').resolve())
# 
#  def testConsistency(self):
#   """Asserts imports work under different patterns"""
#   print(sys.path)
#   with mock.patch("sys.path") as sys_path :
#    print(sys_path)
#   print(sys.path)
# #   result = osexec(self.path, self.mask)
# #   answer = {"class":".MODULE.CLASSA"}
# #   self.assertEqual(answer, result)

pathlib = """
class A():
 '''This is class A'''
"""

class MockModule(Mock, ModuleType) :
 # ModuleType's have the following attributes
 #
 # __dict__
 #
 # ModuleType's have the following methods
 #
 # __delattr__
 # __dir__
 # __getattribute__
 # __init__
 # __new__ 
 # __repr__
 # __setattr__
 #
 # The documentation for ModuleType does not mentions the following attributes (presumably these are part of dict)
 # 
 # __class__
 # __doc__ 
 # __format__
 # __eq__
 # __ge__
 # __gt__
 # __le__
 # __lt__
 # __ne__
 # __new__
 # __reduce__
 # __reduce_ex__
 # __hash__
 # __repr__
 # __sizeof__
 # __str__
 # __subclasshook__    - Interesting ? what does this do ?
 # __init__subclass__
 # 
 def __init__(self, name, *args, doc = None, **kvps):
  super().__init__(name, *args, doc = doc, **kvps)
  package, module = name.split('.',1)
  if package == name :
   package, module  = None, package
   

# @mock.patch("pathlib", module("pathlib.py", "pathlib",pathlib))
# def function(module) :
#  print(dir(module))

if __name__ == "__main__" :
#  main()
#  function()
 with mock.patch("pathlib.__init__", spec_set=types.ModuleType) as module :
  print(module)
  print(type(module))
