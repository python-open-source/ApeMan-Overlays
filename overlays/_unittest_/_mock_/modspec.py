"""
Module Specification
====================

This module is largely a place holder should it become necessary to subclass :class:`importlib.machierny.ModuleSpec`.
Currently this does not seem necessary.
"""
from importlib.machinery import ModuleSpec

class MockSpec(ModuleSpec) :
 """
 DEPRECATED : Tentatively deprecated, subclassing :class:`importlib.machierny.ModuleSpec` provides no benefit at this time.
 """

if __name__ == "__main__" :
 pass
 
