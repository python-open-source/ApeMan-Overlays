"""
Mock
====

This module provides the main mock decorator, :meth:`module`, for the :mod:`overlay.unittest.mock` library.
Eventually this should be merged upstream with :mod:`unittest.mock` and renamed to :meth:`patch.module` but there is still some work to be done before this may happen.

The current implementation method invokes the mock import infrastructure provided to mock a module into existance.
The user is largely limited to creating fictitious modules. 
However these can interfere with modules that are already loaded or be loaded in favour over desired, existing modules.
This results in spurious error messages; Python's import mechanisms seems to squash relevant error messages in favour of disconnected ones.
In most cases this is by design but it makes for finicky debugging.

Future implementations should allow one to patch any existing module in a fashion similar to ApeMan. 
These patches, ideally, would be applied at the module level and not overlap with any that :meth:`unittest.mock.patch.class` and :meth:`unittest.mock.patch.method` might handle.
The patch target here is the module instance itself and it's attributes and not the classes, their methods, or the functions within it.
Granted this might be too tricky to enforce and the user, ultimately, will be responsible for good behaviour.
"""
from .meta import MetaMockFinder

def module(name, path = None, code = None) :
 """
 This should behave like mock.patch, namely provide a module name and then the code for it.
 """
 MetaMockFinder().createModuleSpecification(name, path = path or "", code = code or "")
 def ornament(fxn) :
  return fxn
 return ornament
