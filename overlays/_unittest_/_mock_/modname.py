"""
ModName
=======

This is largely modelled after Python's :mod:`pathlib` and is meant to assist in the management of module names.
"""
import logging; logger = logging.getLogger(__name__)

modsep = '.'

class Module(object) :
 """:class:`Path`\ -like functionality for Python module names
 
 :class:`Module` is modelled after :mod:`pathlib`\ 's :class:`Path`.
 The instances it creates represent the name of a module, with the appropriate manipulation methods.
 """

 @property
 def parent(self):
  return modsep.join(self.path[:-1])

 @property
 def parents(self):
  for i in reversed(range(len(self.path)-1)) :
   yield Module(modsep.join(self.path[:i+1]))
#   traverse = lambda data : (data[:i+1] for i in range(len(data)))
#   for i in reversed(range(len(self.path))) :
#    yield Module(modsep.join(self.path[:i+1]))

 @property
 def level(self) :
  """
  Returns the level of a module. 
  
  e.g.
  
     a.b.c.d  -> 0
     .b.c.d   -> 1
     ..c.d    -> 2
  """
  count,name,parts = 0, [], enumerate(self.path)
  index, part = next(parts)
  while index == count and not part : 
   count+=1
   index, part = next(parts)
  return count

 def __init__(self, name) :
  super().__init__()
  self.path = str(name).split(modsep)

 def __str__(self) :
  return modsep.join(self.path)

 def __hash__(self)  :
  return hash(modsep.join(self.path))

 def __repr__(self) :
  return 'Module("{}")'.format(modsep.join(self.path))

 def __eq__(self, module) :
  return self.path == Module(module).path

 def __call__(self, module) :
  """
  This is setup as a convenience function for calling self.__str__.
  That is, invoking the instance returns a string.
  """
  return str(self)

 def __truediv__(self, module) :
  module = Module(module)
  if True :
   # This code has a somewhat weird behaviour.
   # If the second argument is an absolute path then this is simply returned
   # 
   limits = (len(self.path), module.level, min(len(self.path),module.level))
   prefix = [""] * (max(len(self.path),module.level) - len(self.path)) \
          + self.path[:-min(len(self.path),module.level)]
   suffix = module.path[module.level:]
   return Module(modsep.join(prefix + suffix))
   # Debug line
#    print(*limits, prefix + suffix)
   # Originally
#   if module.level :
#    self.path[:-min(module.level,len(self.path))]
#    self.path[:module.level] + module.path[module.level:]
#   else :
#    self.path[:module.level] + module.path[module.level:]
#   module.path[module.level]
#   self.path[self.level]

 def is_relative(self) :
  return "" in self.path

 def is_absolute(self) :
  return "" not in self.path

 def _resolve_(self) :
  """
  This is a bad knock off of the :meth:`Path.resolve` method.
  :meth:`Path.resolve` determines the absolute path from relative one.
  This is decidedly harder to do in terms of module names unless one may make some asumptions about the current scope or calling scope.
  It is therefore suggested, but not presently implemented, that relative paths be resolved against the current modules :attr:`__package__` information.
  
  It is perhaps better to call this clean or possibly validate.
  """
  count,name = 0, []
  for index, part in enumerate(self.path) :
   if   part == "" and index == count :
    count += 1
    name.append(part)
   elif part == "" :
    pass
   else :
    name.append(part)
  return Module(modsep.join(name))

if __name__ == "__main__" :
 import unittest
 tests = {
  "all"     : 'test*.py',
  "pathlib" : '*Pathlib.py',
  }
 test = "pathlib"
 # Single tests
#  suite = unittest.TestLoader().discover("../..",tests[test])
#  unittest.TextTestRunner(verbosity=1).run(suite)
 # An attempt to filter tests by path
#  suite = unittest.TestLoader().discover('../..',tests[test],"tests/mock")
 # Multiple Tests
 suites = []
 for test in [test] :
  suites.append(unittest.TestLoader().discover("../..",tests[test]))
 unittest.TextTestRunner(verbosity=1).run(unittest.TestSuite(suites))
