"""
Mock'd Modules
==============

This is an attempt to provide Mock'd modules within the Python testing framework.
Allowing one to create loadable modules upon the fly and then drop them as soon as a test/function/context has completed.

There is a good chance that one might get most of what they need done with a simple patch to :attr:`sys.path` and :attr:`sys.modules`, as illustrated in the following example :
::

  from unittest.mock import patch
  from os.path import realpath
  
  @patch("sys.modules", sys.modules.copy())
  @patch("sys.path", [realpath("path/to/project"), *sys.path.copy()])
  def function() :
   import MODULE
   ...
   
This package consists of four modules 

 meta
  This provides the :class:`MetaPathFinder` subclass  :class:`MetMockFinder`, that is prepended to sys.meta_path.
 finder
  This does not exist but may need to be implemented in due course.
 loader
  This provides :class:`MockLoader`, a :class:`importlib.machinery.FileLoader` subclass, that is used for loading mock'd modules.
 mock
  This provides the decorator that one uses to apply/create the module mock ups.
 modname
  This provides :class:`Module` which provides :mod:`pathlib`\ -like functionality for module names.
 module
  This provides the :class:`MockModule` type, a subclass of :class:`types.ModuleType`.
  Presently this is not used anywhere, since :class:`MetaMockFinder` uses :class:`MockLoader` which generates :class:`types.ModuleType` instances instead.
 modspec
  Tentatively provided for future implementations.
  
.. todo :: 

   Rename the following modules accordingly. 
    
    * :file:`machinery.py` :file:`metafinders.py`
    
   Split the following files 
   
    * Extract :class:`MockModule` from :file:`modules` and copy it into :file:`machinery`.

"""
import six
if six.PY2:
 from mock  import *
 from mock  import patch  # This is to make sphinx happy
 # from unittest.mock.__init__ import __all__
 from overlays._unittest_._mock_.mock import module as _module_ # I have no reasun to sunder module other then to emphasize it's hidden
if six.PY3:
 from unittest.mock import *
 from unittest.mock import patch  # This is to make sphinx happy
 # from unittest.mock.__init__ import __all__
 from .mock import module as _module_ # I have no reasun to sunder module other then to emphasize it's hidden

patch.module = _module_

# print(dir())

# __all__ = [
# #  "MockModule",
#  "MetaMockFinder", # Originally : "MockFinder"
#  ]

# ['MagicMock', 'Mock', 'NonCallableMagicMock', 'NonCallableMock', 'PropertyMock', '__builtins__', '__cached__', '__doc__', '__file__', '__loader__', '__name__', '__package__', '__path__', '__spec__', 'call', 'create_autospec', 'loader', 'machinery', 'mock', 'mock_open', 'module', 'patch', 'sentinel']
# print(dir())
# __all__.append("module")

#__all__ = ["module"]
