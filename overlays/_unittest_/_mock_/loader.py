"""
Loader
======

This module provides a loader, :class:`MockLoader`, for the purposes of the MetaPathFinder, :class:`MetaMockFinder`.

.. tikz ::

   \node (EL) {ExecutionLoader};
   \node (RL) {ResourceLoader};
   \node {FL} {FileLoader}; 
   \node {ML} {Mockloader};

ExecutionLoader

This ABC contributes the code necessary to convert source code into compiled code.

ResourceLoader

This handles module data, that is disparate from the module itself e.g. a configuration file, tabular data that is setup via the setup function from setuptools.

 get_data()

FileLoader

This ABC provides the machinery necessary for returning source code from somewhere on a system.
One must implement the following methods

 get_source()
  Returns the source code for the module.

"""
from importlib.abc import FileLoader, SourceLoader

class MockLoader(FileLoader) :
 """
 This class tries to provide for the situation that the code is placed relative to a unit test.
 
 It inherits FileLoader, an abstract base class partially implementing the ResourceLoader and ExecutionLoader ABCs.
 
 .. note ::
 
    This class may fail to do the right thing under certain circumstances, if so compare it to the SourcelessFileLoader implemnetation.
 """
 
 @property
 def code(self):
  if self.path  :
   prefix = "__file__ = '{}'\n".format(self.path) 
   return prefix + self._code_
  else :
   return self._code_

 @code.setter
 def code(self, code):
  self._code_ = code

 def __init__(self, name, *args, path=None, code=None, **kvps): 
  # FileLoader.__init__(self, fullname, path, *args, **kvps)
  """
  Cache the module name and the path to the file found by the finder.  
  
  The parent function blindly accepts two arguments ::`fullname`, renamed to ::`name`, and ::`path` which is stores as instance attributes under the same names respectively.

  .. autofunction :: importlib.abc.FileLoader.__init__
  
  This class accepts an additional attribute caled code which substitutes any code that might be found at :attr:`self.path`
  """
  # Convoluted way to allow optional arguments
#   if not path == ... and code == ... :
#    if os.path.exists(path) :
#     code = ""
#    else : 
#     args.insert(0,path or "")
  super().__init__(name, path or "", *args, **kvps)
  # Note : 
  # If path is None or some empty e.g. "", [],() then it is replaced 
  # with "". Otherwise FileLoader.get_code() fails with a TypeError, 
  # stating "Return a string, bytes or a os.Pathlike object, but not
  # NoneType." when it calls FileLoader.source_to_code() internally.
  self.code = code or """ """ # Consider writing this to a temporary file ?
  # Note : 
  # If code is None or some empty e.g. "", [],() then it is replaced 
  # with a string containing atleast a space or a newline. Otherwise 
  # FileLoader.get_code() fails with a TypeError. 
 
#  def create_module(self, *args, **kvps):
#   print(args, kvps)
#   temp = super().create_module(*args, **kvps)
#   print(temp)
#   print(type(temp))
#   return temp
 
#  def get_filename(self, *args, **kvps):
#   print("Get File Name",args, kvps)
#   temp = super().get_filename(*args, **kvps)
#   print(temp)
#   return temp
 
 def get_source(self, name):
  """
  Returns the source code for the module given the :ref:`FQMN`.
  
  This should return None if there is no code or path.
  Similarly it should return none if name does not match self.name.
  """
  if self.code :
   return self.code
  # Ignores for now as it's not entirely relevant, and one should really treat byte code properly here too.
#   elif self.path : 
#    path = path or self.get_filename(name)
#    return os.path.realtpah(self.path).read()
  else :
   # Some of the get_source functions default to this in the importlib code base.
   return None 

if __name__ == "__main__" :
 from pprint import pprint
 # Testing (Move to proper unit tests)
#  loader = MockLoader("package.module",None,"""class Class() : pass""")
#  print(loader.get_source("package.name"))
#  import inspect
#  inspect(MockLoader.get_code)
 loader = MockLoader("example",None,"")
#  module = loader.load_module()
#  pprint(dir(loader))
#  print(loader.get_source("package.name"))
#  import inspect
#  inspect(MockLoader.get_code)

#  def testGetCode() : 
#   pass
# 
#  def testIsPackage() : 
#   pass
# 
#  def testIsPackage() : 
#   pass
