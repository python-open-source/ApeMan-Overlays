"""
Meta
====

This provides the :class:`MetaMockFinder` class

.. note ::

   `Sankey`_ diagrams and `tracing`_
   
.. _`Sankey`: https://gojs.net/latest/samples/sankey.html
.. _`tracing`: https://pymotw.com/2/trace/
"""
import sys
# import os
# import inspect
import logging; logger = logging.getLogger(__name__)
# import types
# from apeman.descriptors   import PathName
# from apeman.utilities     import Indentation
from importlib.abc        import MetaPathFinder
from importlib.machinery  import ModuleSpec
# from .spec                import Mockspec as ModuleSpec
if __name__ == "__main__" :
 from apeman.mock.loader  import MockLoader # overlays._unittest_._mock_.loader ?
 from apeman.mock.modname import Module
else : 
 from .loader             import MockLoader
 from .modname            import Module

DEBUG = True
modsep = "."

class MetaMockFinder(MetaPathFinder):
 """Meta importer for mock'd modules
 
 This class, a singleton, gets installed upon sys.meta_path when it is first invoked.
 It facilitates the mocking of modules, allowing one to create single use modules on the fly.
 This was originally intended for unit testing purposes but may have application elsewhere.
 
 The aim of this importer is to return ModuleSpec's for the corresponging modules in the order in which they were created, :abbr:`fifo(first in, first out)`.
 Internally when one creates a module, :class:`MetaMockFinder` creates a :class:`ModuleSpec` for it and appends this to a stack, within a dictionery, under the modules name.
 Whenever one imports the given module the name is looked up in the dictionery and the :class:`ModuleSpec` at the top is popped off and returned.
 This contradicts Brett Campbell's view that the import system should not have state but has proved quite practical in unit testing :mod:`apeman`.
 """
 # See section 5.5 in [1] to determine if the Path Based Finder
 # is a better fit for this class
 #
 # https://docs.python.org/3/reference/import.html

#  root = PathName()
#  lom = []

 modules = {}

 _self_ = None

 def __new__(cls, *args, **kvps) :
  if not isinstance(cls._self_, cls) :
   cls._self_ = super().__new__(cls,*args, **kvps)
  return cls._self_

 def __init__(self, *args, mods = {}, **kvps) :
  super().__init__(*args, **kvps)
#   self.modules.append(*mods)
  if self not in sys.meta_path : 
   sys.meta_path.insert(0, self) # Ideally do this when wrapping a function or entering/exiting as a context manager, not sure if freezing sys.path is a good Idea here or not aswell ?

#  def __init__(self, *args, name = None, path = None, root = None, _import_ = __import__, mods = [], **kvps):
#   if DEBUG : logging.debug("OverlayImporter Initialized")
#   super().__init__(*args, **kvps)
#   # Importer Functionality
# #   self.mask = "_{}_"
# #   self.trap = {}
# #   self.wrap = {}
#   self.name = name or inspect.getmodule(inspect.stack()[1][0]).__name__
#   self.root = root or os.path.dirname(inspect.getmodule(inspect.stack()[1][0]).__file__)
#   self.mods = mods #  = self.modules()
#   if DEBUG : logging.debug("{:{}}: {:40} {}".format(self.ondent("Instance"), self.__taglen__, str(self.__class__), [key for key in sys.modules.keys() if self.name in key]))
#   # Import Functionality
# #   builtins.__import__ = self
# #   self.imp = _import_

#  def __call__(self, name, *args) : # (self, *args, *kvps):  
#   # Hooks the import statement
# #   self.log.debug("importing : {}".format(name))
# #   self.log.debug([{arg['__name__']:arg.keys()} if isinstance(arg, dict) else arg for arg in args])
# #   if self.mapToTarget(name) in self.mods.keys() :
# #    self.log.debug("Overloaded")
# #    if name in self.lom :
# #     self.log.debug("unmap : " + name)
# #     return self.imp(name, *args)
# #    self.log.debug("remap : " + name + " -> "+ self.name + "." +self.mapToTarget(name))
# #    self.log.debug(name)
# #    self.log.debug(self.name + modsep + self.mapToTarget(name))
# #    self.log.debug("Wrapped Import")
# #    self.lom.append(name)
# #    return import_module(self.name + modsep + self.mapToTarget(name)) # This is a little black magic as we ignore the args
#   if DEBUG : log.debug("import called : {}".format(name))
#   return self.imp(name, *args)

#  def mapToTarget(self, name) :
#   """Maps request to the overlay module"""
#   # Converts tiers.package.module to _tiers_._package_._module_
#   return modsep.join([self.mask.format(part) for part in name.split(modsep)])

#  def modules(self) : 
#   """ Lists the overlays implemented within a directory """
#   # This differs from overlays in that it recurses through the 
#   # folder structure to find python modules
#   ext = '.py'
#   mod = lambda parts, ext : [part[:-len(ext)] if enum + 1 == len(parts) else part for enum, part in enumerate(parts)]
#   lst = [(mod(file.relative_to(self.root).parts, ext), file) for file in self.root.rglob('*'+ext)]
#   return {modsep.join(item[0][:-1]) if item[0][-1] == "__init__" else modsep.join(item[0]) : item[1] for item in lst}

 def find_spec(self, name, *args, **kvps):
  # Spec attributes
  #
  # name                       - Fully qualified module name (FQMN)
  # parent                     - Enclosing Package
  # submodule_search_locations - Package __path__
  # has_location               - Has External Location
  # origin                     - Source file location
  # cached                     - Cached location
  # loader                     - Loader Object
  #
  module = Module(name)
  if module in self.modules and self.modules[module] :
   logging.debug("Returning : ",name, args, kvps)
   spcs = self.modules[module]
   spec = spcs.pop(0)
   return spec
  else :
   return None 
#   return types.ModuleSpec(module)

#  def find_module(self, *args, **kvps):
#   print("Find Module", args, kvps)
 
 def createModuleSpecification(self, name, *args,  path=None, code=None, **kvps):
  """
  name :
   This represents the name for a module or a package.
  path :
   This represents the path for a module or package.
  code :
   This represents the code for the module or explicit package.

  While :arg:`name` is required, :arg:`path` and :arg:`code` are not.
  :arg:`path` and :arg:`code`, however, are used to determine what loader to use and whether or not a module or a package are loaded.
  Strictly speaking modules and packages are the same of the same type, :class:`types.Module`, but various nuances need to be accounted for.

  Internally the code does the following :
   If :arg:`name` represents a submodule i.e. :arg:`name` contains one or more parts separated by "."; the one must create the packages that would contain it.
  """
  # 
  # I've included the documentation for ModuleSpec.__init__ for reference.
  #
  # ModuleSpec
  #  def __init__(self, name, loader, *, origin=None, loader_state=None, is_package=None)
  #   Initialize self.  See help(type(self)) for accurate signature.
  #
#   path = Path(path) if path else None
  # Helper Functions
  # Process Inputs
  module = Module(name)
  # Module Specification
  for parent in module.parents :
   if parent in self.modules : # This test protects preceeding setups, not sure if it's useful or detrimental.
    logger.info("Update Parent : {}".format(parent))
    self.modules[parent].append(ModuleSpec(str(parent), MockLoader(str(parent), *args, path = path, code = code,**kvps), is_package=True))
   else :
    logger.info("Create Parent : {}".format(parent))
    self.modules[parent] = [ModuleSpec(str(parent), MockLoader(str(parent), *args, path = path, code = code, **kvps), is_package=True)]
  if module in self.modules : # This test protects preceeding setups, not sure if it's useful or detrimental.
   logger.info("Update Module : {}".format(module))
   self.modules[module].append(ModuleSpec(str(module), MockLoader(str(module), *args, path = path, code = code, **kvps), is_package=False))
  else :  
   logger.info("Create Module : {}".format(module))
   self.modules[module] = [ModuleSpec(str(module), MockLoader(str(module), *args, path = path, code = code, **kvps), is_package=False)]

if __name__ == "__main__" :
 # Call Test Suites
 import unittest
 tests = {
  "all"    : 'test*.py',
  "mock"   : '*Mock.py',
  }
 test = "module"
 # Single tests
#  suite = unittest.TestLoader().discover("../..",tests[test])
#  unittest.TextTestRunner(verbosity=1).run(suite)
 # An attempt to filter tests by path
#  suite = unittest.TestLoader().discover('../..',tests[test],"tests/mock")
 # Multiple Tests
 suites = []
 for test in ["mock"] :
  suites.append(unittest.TestLoader().discover("../..",tests[test]))
 unittest.TextTestRunner(verbosity=1).run(unittest.TestSuite(suites))

 # Test that the MetaMockFinder can create and return plain modules
#  from pprint import pprint
#  meta = MetaMockFinder() 
#  meta.createModuleSpecification("example", code = "")
#  # import example 
#  from importlib.util import find_spec
#  spec = find_spec("example")
# #  print(spec)
# #  pprint(dir(spec.loader))
#  spec.loader.load_module()

 # Test that MetMockFinder inserts itself into sys.meta_path
#  print(sys.meta_path)
#  mmf = MetaMockFinder(modules = [MockSpec()])
#  print(sys.meta_path)
#  sys.meta_path
