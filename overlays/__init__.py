"""
The :mod:`overlay.__init__` file imports :mod:`apeman` and instantiates :class:`apeman.Apeman` for the user.
:class:`apeman.Apeman` installs itself upon the python import path enabling all of the overlays provided by this package.
Later imports are then caught by :class:`apeman.Apeman` and substituted by the relevant overlays.
"""
from apeman import ApeMan

apeman = ApeMan()
