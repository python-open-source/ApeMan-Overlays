"""
Module
======

.. note ::

  :class:`types.ModuleType` is sufficiently flexible to void the usage of :class:`MockModule` at this time.
  MockModule is retained for now rather then deprecated more for sentimental reasons then anything else.
"""
import types
import inspect
from pathlib import Path

class MockModule(types.ModuleType) :
 """The mock up of a module
 
 This class mocks a Python module in what is hoped to be the most sensible/practical manner possible.
 
 The user may supply the source for a module directly or via a file. 
 Specifying both makes the source appear to come from the specified file, which may or may not exist.
 To setup multiple variations of a module one should specify a relative path for the file name and alter the root accordingly.
 
 .. One may supply the module's code directly as an argument or via a file upon the system relative to the calling code.
 .. If the user suplies both the path is treated as fictitious.
 
 Example :
  The simplest invocation mimics that of :class:`types.ModuleType`, creating an empty module instance.
  ::
    MockModule("MODULE"[, doc = "DOCUMENTATION"])
    types.ModuleType("MODULE"[, doc = "DOCUMENTATION"])
 
 Example :
  One may populate the module with actual code, either directly or from a file as follows
  ::
    MockModule("MODULE"[, doc = "DOCUMENTATION"], code = "CODE")
    MockModule("MODULE"[, doc = "DOCUMENTATION"], file = "path/to/file.py")
 
 Example :
  One may further wish to make the specified code appear to come from a particular file, notably one that does not actually exist.
  ::
    MockModule("MODULE"[, doc = "DOCUMENTATION"], code = "CODE", file = "path/to/file.py")
 
 Example :
  Otherwise one might want to create variations of a given module and store them in parallel folders.
  ::
    MockModule("MODULE"[, doc = "DOCUMENTATION"], file = "path/to/file.py", root = "root/variation_a")
    MockModule("MODULE"[, doc = "DOCUMENTATION"], file = "path/to/file.py", root = "root/variation_b")
 
 Arguments
  name :
   The name of the module, as per :class:`types.ModuleType`.
  doc :
   The optional doc string for the module, as per :class:`types.ModuleType`
  root :
   The root folder from which imports may be made. 
   This defaults to the folder the code invoking MockModule resides in. 
  path :
   If path is relative the module tha is loaded is loaded relative to root.
  code :
   The code for the module, this overrides any code that is present in a file at the given path.
   
 note :
  Namespaces packages/modules are not necessarily supported.
  
 note :
  At 2:05 David beazeley's Live and Let die discusses lazy module loading. 
  Mock allows one to return different results for repeated function calls.
  Both effects could be combined whereby a user import MODULE.OBJECT could get alternative TYPES for the same OBJECT.
  This obviously is a little convoluted but I can think of one usecase for this for my QtTree library, where TreeData returns mutates subclasses of itself.
 """

 def __init__(self, name, doc=None, path = None, code = None, root = None):
  # Attribute   Description
  # =========== ====================
  # __name__    Name
  # __file__    Source file if any
  # __doc__     documentation string
  # __path__    package path
  # __package__ package name
  # __spec__    ModuleSpec
  # 
  # __name__ and __doc__ are handled by the super call.
  # name is split into __package__ and __name__, perhaps incorrectly so.
  # __file__ is determined from root and path together, presumably this is the most flexible from a mocking stand point.
  # code overwrites anything that might be found in __file__
  partname = lambda parts : (*parts, None) if len(parts) == 1 else parts
  pkg, mod = partname(name.rsplit(".",1))
  super().__init__(mod, doc=doc)
  self.__package__ = pkg
  # File handling
  root = root or Path(inspect.getmodule(inspect.stack()[1][0]).__file__).parent
  path = path or Path(*name.split(modsep))
  self.__file__ = root/path
  # Code handling
  try : 
   code = code or self.__file__.read_text()
   temp = compile(code, self.__file__, 'exec')
   exec(temp, self.__dict__)
  except FileNotFoundError as error : 
   pass # Ignore missing files, the module is being Mocked.
#   self.path = Path(path)
#   self.code = code
#  mod = types.ModuleType(name)
#  src = compile(code, file, "exec")
#  exec(src, mod.__dict__)
#  return mod

# print(dir(MockModule("test", code = """
# class A():
#  def __str__(self):
#   return "Frikkin A"
# print(A())
# """)))

if __name__ == "__main__" :
 # Call Test Suites
 import unittest
 tests = {
  "all"    : 'test*.py',
  "module" : '*Module.py',
  "mock"   : '*Mock.py',
  }
 test = "module"
 # Single tests
#  suite = unittest.TestLoader().discover("../..",tests[test])
#  unittest.TextTestRunner(verbosity=1).run(suite)
 # An attempt to filter tests by path
#  suite = unittest.TestLoader().discover('../..',tests[test],"tests/mock")
 # Multiple Tests
 suites = []
 for test in ["module","mock"] :
  suites.append(unittest.TestLoader().discover("../..",tests[test]))
 unittest.TextTestRunner(verbosity=1).run(unittest.TestSuite(suites))
