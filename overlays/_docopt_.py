"""
======
DocOpt
======

.. note :: 

   The docs for the module are actually under `funparts`.

.. note :: 

   Docopt has it's own dict class that it returns. If this is
   ordered or structured in some way I am not aware of this 

Introduction
============

Docopt is quite good at normalizing the arguments it parses e.g. it will convert single letter arguments, "-h", to their full length equivalents, "--help".
Docopt, however, will not strip off any superfluous characters nor ensure the correct type for arguments.

This overlay provides further functionality to normalize the docopt arguments, convert them to the appropriate type and pack them in the same order anticipated by a function.
These features are made available through the following suite of functions 

 * The following functions are concerned with the structure of the options returned by docopts. 

   :meth:`clean`     - This will generate a mapping where the keys match thos in the options returned by docopt and the values represent the cleaned version of the eys.
   This will strip of any superfluous characters e.g. {"--help":value} -> {"--help":"help"}.
                     
   :meth:`retag`     - This will apply the cleaned tags from :meth:`clean` to the docopt options e.g. {"--help":value} -> {"help":value}.
   
   :meth:`funinit`   - This applies a set of functions to the docopt structure converting all of the values to the appropriate type.
 
 * The next set of fucntions examine the call signature of the function being invoked and map the options from docopt to the function being called.
 
   :meth:`funparts`  - This is used to generate a list of arguments and keyword arguments from the function signature.
   It then applies a set of *default* arguments to theses structures and returnes a "populated" set of arguments for the function.
                      
   :meth:`split`     - This takes the populated set of arguments from funparts an overlays the docopt options.

 * The above functions are then combined together in the following function(s)
 
   :meth:`normalize` - This accepts a function, a set of defaults and a set of conversion functions into a single function
   
Typically the use would only want to use :meth:`normalize` to invoke their :meth:`main` method with the options parsed by docopt.
   
Usage
=====

The following example illustrates how to use the module. 
The example is necessarily obscured to ensure its atleast a representative example.
::

  switches  = {**dict.fromkeys(['recurse','verbose'],True), 
               **dict.fromkeys(['suffix','prefix'],False)}
  default   = {"root":Path.cwd(), "glob":"**/*"}
  arguments = []
  normalize(main, opts, *arguments, **default, **switches)

.. topic :: Example : Normalizing docopts' command line arguments
  
  Consider a 'search' package that lets one find things from 
  the command line by invoking `python -m search`. To provide
  such functionality one includes a `__main__.py` file within
  their package.

  Now `__main__.py` parses the commandline arguments with the
  `dopcopt` function which it the passes to a function called
  `main` which calls either `pathSearch` or `fileSearch`. If 
  we assume the following signatures for these functions
  ::
  
    main(*args, flag = False, **kvps)
    fileSearch(term, root = Path.cwd(), __depth__=0)
    pathSearch(term, root = Path.cwd(), recurse = False, __depth__ = 0)
    
  Where `__depth__`, `term`, `recurse` and `root` are common
  to both search functions. `__depth__` is considered part of
  the internal API and `flag` is required  only by main.
  
  The corresponding `__doc__` string for the package would 
  likely read as follows 
  ::
  
    '''
    Search 
    
    A package to make it convenient to find stuff
    
    Usage:
     search [-fr] TERM [<Root>]
    
    Arguments:
     term  The term to search for
     root  The initial file or path
    
    Options:
     -f --flag  Either file or path search [default: True]
     -r         Enable recursive searches [default: False]
    '''

  which `docopt` would parse into the following representation
  ::
  
    opts = {'--flag':False, 
            '-r':False, 
            '<Root>':None, 
            'TERM':None}
  
  Sadly none of the keys match the call signatures. To remedy 
  this one should call `normalize`
  ::
  
    normalize(main, opts, 'term', root=os.cwd(), recurse=False)
    
  which will inspect `main`s' call signature identifying `flag`
  as a further keyword argument to be combined with `root` and
  `recurse` and `term` as the only positional argument. These
  are then combined together and `main` is called accordingly
  ::
  
    main(term, root=root, recursive=recursive, flag = flag)
    
  The astute reader might notice some discrepencies within the  
  conversion. The reality is that the following call is made.
  ::
  
    main(opts['TERM'], Root=opts['<Root>'], r=opts['-r'], flag = opts['--flag'])
    
  Which would result in a number of errors. To fix this one can
  apply a mapping to opts. To create one use `clean` which one
  may update to deal with the messy keys. Then one `retag`s 
  `opts`. One may then call `normalize` as before.
  ::
  
    tags = clean(opts)
    tags.update({'Root':'root','r':'recurse'})
    opts = clean(opts, tags)
    
  The reason this happens is that terms in braces `<...>` have
  their case protected by `clean`. Furhtermore neither `docopt`
  nor `normalize` know that `r` should map to `recurse`. Since
  both a long and short form are specified for `flag` `docopt`
  would convert that for us.
"""
# This comes from the PyREx package and should be removed from
# there if it works
from docopt import *
import inspect

def normalize(__function__, __options__, *args, **kvps) :
 """
 This executes a function with the options and arguments that 
 are generated from `docopt`, along with any additionally named 
 positional or keyword arguments
 """
#  print("Norm")
#  print(args, kvps)
 args, kvps   = funparts(__function__, *args, **kvps)
 args, kvps,_ = split(retag(__options__, clean(__options__)), args, kvps)
#  print(args, kvps)
 return __function__(*args, **kvps)
 
def funparts(__function__, *args, **kvps) :
 """
 This considers a functions call signature and maps additional
 positional and keyword arguments to the function being invoked. 
 """
 # If `funparts` had the following call signature
 # ::
 #   funparts(fun, opts, *args, **kvps)
 # then calling it with the following arguments
 # ::
 #   funparts(FUNCTION, OPTIONS, 'named', opts = VALUE)
 # would fail since `opts` is used twice, both as the second 
 # positional argument in the call signature and as a keyword
 # argument when it is invoked. To correct this the following
 # call signature is used
 # ::
 #   funparts(__function__, __options__, *args, **kvps)
#  Consider the following invocation of a script
#   python -m script COMMAND -o OPT -option OPTION ARGUMENT
#  which docopt would convert into 
#   {"-o":"","":""} 
 spec = inspect.getfullargspec(__function__)
 rem ={}
 if spec.defaults :
  fargs = (spec.args[:-len(spec.defaults)] or [])
  fkvps = dict(zip(spec.args[-len(spec.defaults):], spec.defaults)) or {}
 else :
  fargs = (spec.args or [])
  fkvps = spec.kwonlydefaults or {}
 args = [ *fargs,  *args]
 kvps = {**fkvps, **kvps} # kvps overwites the functions' kvps
 return args, kvps

def split(opts, args, kvps):
 """
 Substitute the entries of a list and a dictionery with the 
 corresponding ones in another dictionery
 
 the output is structured in the way that it is so that one 
 might collect all the later terms together.
 """
 # The following is probably better done with
 # args = [D.pop(k) if k in D else k for k in l]
 # kvps = [d.update({k: D.pop(k)}) for k in d.keys() if k in D]
 # return args, kvps, opts
 rem = {}
 for key, val in opts.items() :
  if   key in args :
   args[args.index(key)] = val
  elif key in kvps : 
   kvps[key] = val
  else :
   rem[key] = val
 return args, kvps, rem
 
def clean(opts, lower = True, braces = True, endash = True, emdash = True):
 """
 This cleans up the keys in the docopts dictionery, by removing
 the superfluous characters in the dictionerys' keys.

 This is my second attempt at normalizing the keys of docopts'
 dict, this seems more robust then the original normalize
 """
 opts = {k:k for k in opts}
 if lower : 
  opts = {key: val.lower() if key.isupper() else val for key, val in opts.items()}
 if braces : # Deliberately placed after lower so that one may preserver case if necessary
  opts = {key: val.lstrip('<').rstrip('>') if key.startswith("<") and key.endswith(">") else val for key, val in opts.items()}
 if endash :
  opts = {key: val.lstrip('-') if key.startswith("-") and not key.startswith("--") else val for key, val in opts.items()}
 if emdash :
  opts = {key: val.lstrip('-') if key.startswith("-") else val for key, val in opts.items()}
 return opts 
 
def retag(opts, tags, trim = False) :
 """
 Given opts and tags rename all the keys in opts according to 
 those in tags.
 
 One may also optionally filter by the same tags, that is any 
 key/value pair not present in taks will be filtered out of 
 opts. This is useful for splitting the opts up aswell.
 """
 if trim :
  return {tags[key] : val for key,val in opts.items() if key in tags}
 else :
  return {tags[key] if key in tags else key : val for key,val in opts.items()}

def funinit(data, fxns) :
 """
 This compares the keys in both fxns and data to one another. 
 Whenever two keys are the same it calls the function in fxns
 on the data in data and substitutes in the result.
 """
 # This should really be merged with normalize as the functions can be made to return default values aswell.
 for key in set(fxns.keys()).intersection(set(data.keys())) :
#   print(key)
  data[key] = fxns[key](data[key])
 return data 

## Previous invocation/usage 
#
# This is placed here for incorporation to the documentation
#
# if __name__ == "__main__":
#  args, kvps, opts = split(retag(opts, clean(opts)), *funparts(main, *arguments, **default))
#  print(args, kvps, opts)
