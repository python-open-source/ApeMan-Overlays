"""
This extends pathlib by allowing a variant of glob, flob, to be
patched into the pathlib.Path class
"""
from fnmatch import fnmatch
from pathlib import *

def flob(root=Path('.'), glob = "**", depth = [], index = False):
 """
 Flob is a variant of glob that returns folders aswell, hence 
 the name. 
 
 The usual pattern matching is supported, that is the recursive 
 form '**' is supported. This is possible since glob, and flob, 
 use fnmatch under the hood. Similarly Path.(r)glob is really 
 glob under the hood. 
 
 flob returns the folder(s) and files in a sorted depth first
 search order.
 
 This is probably more closely related to os.walk but uses the
 pathlib Path API/Objects. 
 
 Arguments :
  root  - This is the root path to start searching from
  glob  - This is the file name pattern that matches the files
          in the folder.
  depth - This is the tuple which comprises the index later on
  index - returns an ordered tuple along with the path which 
          indicates file or folder order e.g. (3,2,5) is the 
          fifth file or folder in teh second folder of the 
          third folder. 
          
 .. Note ::
 
   Index is redundant and the user should wrap flob within an
   enumerate call instead if they require unique indices.
 """
 # This should become part of an overly for Pathlib. The index 
 # feature should then become srapped and 
 cntr = 0
 for path in sorted(root.iterdir(), key= lambda p:(not p.is_dir(), str(p))):
  if path.is_dir() :
   yield (path, (*depth, cntr)) if index else path
   cntr += 1
   if glob.startswith("**"):
    yield from flob(path, glob, (*depth, cntr))
  elif fnmatch(path, glob) :
   yield (path, (*depth, cntr)) if index else path
   cntr += 1

class Path(Path):

 flob = flob
 
if __name__ == "__main__" :
 from pprint import pprint
 pprint(list(flob()))
